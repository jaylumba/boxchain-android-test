package com.jcl.boxchain.exceptions

class ProgressLayoutException(ex: Exception) : BaseException(ex)
