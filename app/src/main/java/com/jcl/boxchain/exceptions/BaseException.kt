package com.jcl.boxchain.exceptions

open class BaseException(cause: Throwable) : RuntimeException(cause)
