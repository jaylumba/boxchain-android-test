package com.exist.phr.di

import com.jcl.boxchain.ui.main.MainActivity
import com.jcl.boxchain.ui.main.di.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by jaylumba on 11/18/2017.
 */

@Module
abstract class BuildersModule{

    @ContributesAndroidInjector(modules = arrayOf(MainModule::class))
    internal abstract fun bindMainActivity(): MainActivity

}
