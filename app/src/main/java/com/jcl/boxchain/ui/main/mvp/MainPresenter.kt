package com.jcl.boxchain.ui.main.mvp;

import android.annotation.SuppressLint
import com.exist.phr.data.api.DataService
import com.exist.phr.data.api.ViewService
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jcl.boxchain.base.BasePresenter
import com.jcl.boxchain.data.model.Element
import com.jcl.boxchain.ui.main.MainActivity
import com.jcl.boxchain.util.scheduler.SchedulerProvider
import io.reactivex.Observable
import timber.log.Timber


class MainPresenter(private val schedulerProvider: SchedulerProvider,
                    view: MainContract.View,
                    private val viewService: ViewService,
                    private val dataService: DataService) :
        BasePresenter<MainContract.View>(view), MainContract.Presenter {

    private val TAG = MainActivity::class.java.simpleName
    private val requestStateObserver = BehaviorRelay.createDefault(BasePresenter.RequestState.IDLE)

    init {
        observeRequestState()
    }

    private fun publishRequestState(requestState: BasePresenter.RequestState) {
        addDisposable(Observable.just(requestState)
                .observeOn(schedulerProvider.ui())
                .subscribe(requestStateObserver))
    }

    @SuppressLint("CheckResult")
    private fun observeRequestState() {
        requestStateObserver.subscribe({ requestState ->
            when (requestState) {
                BasePresenter.RequestState.IDLE -> {
                }
                BasePresenter.RequestState.LOADING -> view.setLoadingIndicator(true)
                BasePresenter.RequestState.COMPLETE -> view.setLoadingIndicator(false)
                BasePresenter.RequestState.ERROR -> view.setLoadingIndicator(false)
            }
        }, { Timber.e(it) })
    }

    override fun onGetViewElements() {
        addDisposable(viewService.getViewElements()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { publishRequestState(RequestState.LOADING) }
                .doOnSuccess { publishRequestState(RequestState.COMPLETE) }
                .doOnError { publishRequestState(RequestState.ERROR) }
                .subscribe({ response ->
                    view.setTitle(response.activityTitle)
                    response.elements.forEach { onGetData(it) }
                }, { throwable ->
                    view.showToastError(throwable.localizedMessage)
                }))
    }

    private fun onGetData(element: Element) {
        addDisposable(dataService.getData(element.data)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { publishRequestState(RequestState.LOADING) }
                .doOnSuccess { publishRequestState(RequestState.COMPLETE) }
                .doOnError { publishRequestState(RequestState.ERROR) }
                .subscribe({ response ->
                    view.displayElements(element, response.users)
                }, { throwable ->
                    view.showToastError(throwable.localizedMessage)
                }))
    }
}