package com.jcl.boxchain.ui.main.mvp;

import com.jcl.boxchain.base.BaseView
import com.jcl.boxchain.data.model.Element
import com.jcl.boxchain.data.model.User


interface MainContract {
    interface View : BaseView {
        fun setTitle(title: String)
        fun displayElements(element: Element, data: ArrayList<User>)
    }

    interface Presenter {
        fun onGetViewElements()
    }
}