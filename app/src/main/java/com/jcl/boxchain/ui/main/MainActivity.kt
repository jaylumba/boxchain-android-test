package com.jcl.boxchain.ui.main

import android.os.Bundle
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindDimen
import butterknife.ButterKnife
import com.google.gson.Gson
import com.jcl.boxchain.R
import com.jcl.boxchain.base.BaseActivity
import com.jcl.boxchain.data.enums.FieldType
import com.jcl.boxchain.data.model.Element
import com.jcl.boxchain.data.model.User
import com.jcl.boxchain.ui.main.mvp.MainContract
import com.jcl.boxchain.ui.main.mvp.MainPresenter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import javax.inject.Inject


class MainActivity : BaseActivity(), MainContract.View {

    @JvmField
    @BindDimen(R.dimen.spacing_base)
    internal var spacingBase: Int = 0

    @Inject
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        ButterKnife.bind(this)
        presenter.onGetViewElements()
    }

    override fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun displayElements(element: Element, data: ArrayList<User>) {
        data.forEach { user ->
            //Create layout params
            val layoutParams: ViewGroup.MarginLayoutParams = ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            layoutParams.setMargins(spacingBase, 0, spacingBase, spacingBase)

            //Create linearlayout that will hold the users data
            val llUserContainer = LinearLayout(this)
            llUserContainer.orientation = LinearLayout.VERTICAL
            llUserContainer.setPadding(spacingBase, spacingBase, spacingBase, spacingBase)
            llUserContainer.layoutParams = layoutParams
            llUserContainer.setBackgroundResource(R.color.colorWhite)

            //Display data
            element.fields.forEach { field ->
                when (field.type) {
                    FieldType.TEXTVIEW.toString() -> {
                        val tvData = TextView(this)
                        val jsonObject = JSONObject(Gson().toJson(user))
                        if (jsonObject.has(field.id) && !jsonObject.getString(field.id).isNullOrBlank()){
                            tvData.text = field.label.plus(": ").plus(jsonObject.getString(field.id))
                            llUserContainer.addView(tvData)
                        }
                    }
                    FieldType.EDITTEXT.toString() -> {
                        //TODO: Implement once available
                    }
                }
            }

            //Add to parent view
            if (llUserContainer.childCount > 0)
                llElementContainer.addView(llUserContainer)
        }
    }
}
