package com.jcl.boxchain.ui.main.di;

import com.exist.phr.data.api.DataService
import com.exist.phr.data.api.ViewService
import com.jcl.boxchain.ui.main.MainActivity
import com.jcl.boxchain.ui.main.mvp.MainContract
import com.jcl.boxchain.ui.main.mvp.MainPresenter
import com.jcl.boxchain.util.scheduler.AppSchedulerProvider
import dagger.Module;
import dagger.Provides;


@Module
class MainModule {

    @Provides
    internal fun provideView(activity: MainActivity): MainContract.View {
        return activity
    }

    @Provides
    internal fun providePresenter(appSchedulerProvider: AppSchedulerProvider, view: MainContract.View,
                                  viewService: ViewService, dataService: DataService): MainPresenter {
        return MainPresenter(appSchedulerProvider, view, viewService, dataService)
    }
}