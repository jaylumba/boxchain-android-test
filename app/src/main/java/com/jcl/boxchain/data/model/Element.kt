package com.jcl.boxchain.data.model

data class Element(var id: String,
                   var type: String,
                   var data: String,
                   var fields: List<Field>)