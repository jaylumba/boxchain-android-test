package com.jcl.boxchain.data.enums

enum class FieldType(val type: String) {
    TEXTVIEW("textView"),
    EDITTEXT("editText");

    override fun toString(): String {
        return this.type
    }

    fun equalsType(otherType: String): Boolean {
        return type == otherType
    }
}