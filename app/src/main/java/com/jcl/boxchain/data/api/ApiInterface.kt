package com.exist.phr.data.api

import com.jcl.boxchain.data.dto.GetDataResponseDto
import com.jcl.boxchain.data.dto.GetViewElementResponseDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url



/**
 * Created by jaylumba on 05/16/2018.
 */

interface ApiInterface {
    @GET("v2/5afaa7cd2e00005000279004")
    fun getViewElement():Single<GetViewElementResponseDto>

    @GET
    fun getData(@Url url: String): Single<GetDataResponseDto>

}
