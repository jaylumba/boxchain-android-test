package com.jcl.boxchain.data.model

data class Field(var id: String,
                 var label: String,
                 var type: String)