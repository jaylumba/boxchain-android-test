package com.jcl.boxchain.data.dto

import com.jcl.boxchain.data.model.User

data class GetDataResponseDto (var users: ArrayList<User>)