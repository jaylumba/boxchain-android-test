package com.jcl.boxchain.data.model

data class User (var id: String,
                 var fullName: String,
                 var age: String,
                 var contactNo: String,
                 var email: String)