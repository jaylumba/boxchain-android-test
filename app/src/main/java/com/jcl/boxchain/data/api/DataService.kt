package com.exist.phr.data.api

import com.jcl.boxchain.data.dto.GetDataResponseDto
import io.reactivex.Single

/**
 * Created by jaylumba on 05/16/2018.
 */
class DataService(private var apiInterface: ApiInterface) {
    fun getData(url: String): Single<GetDataResponseDto> {
        return apiInterface.getData(url)
    }
}