package com.jcl.boxchain.data.enums

enum class ElementType(val type: String) {
    LIST("list");

    override fun toString(): String {
        return this.type
    }

    fun equalsType(otherType: String): Boolean {
        return type == otherType
    }
}
