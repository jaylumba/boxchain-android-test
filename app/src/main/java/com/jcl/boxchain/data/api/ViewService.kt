package com.exist.phr.data.api

import com.jcl.boxchain.data.dto.GetViewElementResponseDto
import io.reactivex.Single

/**
 * Created by jaylumba on 05/16/2018.
 */
class ViewService (private var apiInterface: ApiInterface){

    fun getViewElements(): Single<GetViewElementResponseDto> {
        return apiInterface.getViewElement()
    }

}