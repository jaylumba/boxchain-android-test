package com.jcl.boxchain.data.dto

import com.jcl.boxchain.data.model.Element

data class GetViewElementResponseDto (var activityTitle: String,
                                      var elements: List<Element>)